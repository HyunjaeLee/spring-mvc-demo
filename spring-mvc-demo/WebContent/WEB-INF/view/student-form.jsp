<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
<title>Student Registration Form</title>
</head>

<body>
	<form:form action="processForm" modelAttribute="student">
		
		First name : <form:input path="firstName" />
		
		<br><br>
		
		Last name : <form:input path="lastName" />
		
		<br><br>
		
		Country:
		<!-- On submit, Spring will call student.setCountry(...) -->
		<form:select path="country">
			
			<form:options items="${theCountryOptions}" />
		
		
			<%-- <!-- Spring will call student.getCountryOptions() -->
			<form:options items="${student.countryOptions}"/> --%>
			
			<%-- <form:option value="Brazil" label="Brazil"/>
			<form:option value="France" label="France"/>
			<form:option value="Germany" label="Germany"/>
			<form:option value="India" label="India"/> --%>
		</form:select>
		
		<br><br>
		
		Favorite Language:
		
		<!-- On submit, Spring will call student.setFavoriteLanguage -->
		<%-- Java <form:radiobutton path="favoriteLanguage" value="Java"/>
		C# <form:radiobutton path="favoriteLanguage" value="C#"/>
		Python <form:radiobutton path="favoriteLanguage" value="Python"/>
		Ruby <form:radiobutton path="favoriteLanguage" value="Ruby"/>
		 --%>
		 
		<form:radiobuttons path="favoriteLanguage" 
				items="${student.favoriteLanguageOptions}"  />
		<br><br>
		
		Operating Systems:
		<!-- On submit, Spring will call student.setOperatingSystems -->
		Linux <form:checkbox path="operatingSystems" value="Linux"/>
		Mac OS <form:checkbox path="operatingSystems" value="Mac OS"/>
		Windows <form:checkbox path="operatingSystems" value="Windows"/>
		
		<br><br>
		
		<input type="submit" value="Submit" />
		
	</form:form>
</body>

</html>